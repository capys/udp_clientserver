#ifndef MYUDP_H
#define MYUDP_H

#include <QObject>
#include <QUdpSocket>


class MyUDP : public QObject
{
    Q_OBJECT
public:
    explicit MyUDP(QObject *parent = nullptr);
    void ReadRawUdp( QString hostaddr, quint64 port);
    void SendRawUdp(QString hostaddr, quint64 port, QByteArray data);

signals:

public slots:
    void readyRead();

private:
    QUdpSocket * socket;

};

#endif // MYUDP_H

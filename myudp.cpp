#include "myudp.h"

MyUDP::MyUDP(QObject *parent) : QObject(parent)
{
    socket = new QUdpSocket(this);
    connect(socket,SIGNAL(readyRead()),this,SLOT(readyRead()));
}

void MyUDP::ReadRawUdp(QString hostaddr, quint64 port){
    socket->bind(QHostAddress(hostaddr),port);
    //socket->writeDatagram(Data,QHostAddress(hostaddr),port);
    //192.168.1.255 broadcast address, local subnet
}


void MyUDP::SendRawUdp(QString hostaddr, quint64 port, QByteArray data){
    socket->bind(QHostAddress(hostaddr),port);
    socket->writeDatagram(data,QHostAddress::LocalHost,8000);

}
void MyUDP::readyRead(){
    QByteArray Buffer;
    Buffer.resize(socket->pendingDatagramSize());
    
    QHostAddress sender;
    quint16 senderPort;
    socket->readDatagram(Buffer.data(),Buffer.size(),&sender,&senderPort);

   qDebug() <<"From IP: "<<sender.toString()<<"From Port: "<<senderPort<<"Length: "<<Buffer.toHex().length()<<"Hex: "<< Buffer.toHex()<<"Message: "<< Buffer.data();
}

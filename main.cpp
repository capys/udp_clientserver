#include <QCoreApplication>
//#include "myserver.h"
#include "myudp.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    MyUDP server;
    MyUDP client;

    server.SendRawUdp("localhost",8000,"Hello from UPD land");
    client.ReadRawUdp("localhost",8000);

    return a.exec();
}
